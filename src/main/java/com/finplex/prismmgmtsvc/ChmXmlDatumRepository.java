package com.finplex.prismmgmtsvc;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

@Repository
public interface ChmXmlDatumRepository extends CrudRepository<ChmXmlDatum, Serializable> {
    @Query("SELECT c.xmlResponse FROM ChmXmlDatum c WHERE c.reportId in (?1)")
    List<String> findByReportids(List<String> reportids);

}
