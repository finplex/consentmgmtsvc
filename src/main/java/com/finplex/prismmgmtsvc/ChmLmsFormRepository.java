package com.finplex.prismmgmtsvc;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

@Repository
public interface ChmLmsFormRepository extends CrudRepository<ChmLmsForm, Serializable> {
    @Query(value = "SELECT c.report_id FROM chm_lms_form c WHERE c.loan_id = :loanid", nativeQuery = true)
    List<String> findReportidforloanid(@Param("loanid") String loanid);
}