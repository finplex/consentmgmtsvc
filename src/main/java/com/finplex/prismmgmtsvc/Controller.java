package com.finplex.prismmgmtsvc;

import com.finplex.prismmgmtsvc.services.BureauService;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;
import java.util.Optional;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/prismmgmtsvc")
public class Controller {

    @Autowired
    ChmLmsFormRepository chmLmsFormRepository;
    @Autowired
    ChmXmlDatumRepository chmXmlDataRepository;

    @GetMapping("/health")
    public String get() {
        return "ok";
    }

    @GetMapping(value = "/getuserdata", produces = MediaType.APPLICATION_JSON_VALUE)
    public Optional<ChmLmsForm> getuserdata(@RequestHeader("x-api-key") String apiKey, @RequestParam Integer id){
        return chmLmsFormRepository.findById(id);
    }

    @GetMapping(value = "/getuserbureau", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<String> getuserbureau(@RequestHeader("x-api-key") Optional<String> apiKey, @RequestParam String loanid){
        List<String> repordids = chmLmsFormRepository.findReportidforloanid(loanid);
        List<String> resp = (List<String>) chmXmlDataRepository.findByReportids(repordids);
        return resp;
    }
    @GetMapping(value = "/getuserbureauasFile", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<String> getuserbureauasFile(@RequestHeader("x-api-key") Optional<String> apiKey, @RequestParam String loanid, HttpServletResponse response) throws IOException {
        List<String> repordids = chmLmsFormRepository.findReportidforloanid(loanid);
        List<String> resp = (List<String>) chmXmlDataRepository.findByReportids(repordids);
        File zipfile = new File("./Bureau.zip");
        FileOutputStream fileOutputStream = new FileOutputStream(zipfile);
        ZipOutputStream zipOutputStream = new ZipOutputStream(fileOutputStream);//response.getOutputStream());
        // allFileNames.forEach(f -> {
        for (int i =0;i<resp.size();i++) {
            String fileName = "./" + loanid + "_bureau"+i+".xml";
            BureauService.writeStringToFile(resp.get(i), fileName);
            FileSystemResource fileSystemResource = new FileSystemResource(fileName);
            ZipEntry zipEntry = new ZipEntry(fileSystemResource.getFilename());
            try {
                zipEntry.setSize(fileSystemResource.contentLength());
                zipOutputStream.putNextEntry(zipEntry);
                InputStream fis = fileSystemResource.getInputStream();
                IOUtils.copy(fis, zipOutputStream);
                fis.close();
                zipOutputStream.closeEntry();
            } catch (IOException e) {
                e.printStackTrace();
            }
            //  });
        }
        zipOutputStream.finish();
        ;
        zipOutputStream.close();

        File zipFile = new File("./Bureau.zip");
        response.setContentType("application/zip");
        response.setHeader("Content-Length", String.valueOf(zipFile.length()));
        response.setStatus(HttpStatus.OK.value());
        response.setHeader("Content-Disposition", "attachment; filename=\"" + "Bureau.zip" + "\"");
        InputStream is = new FileInputStream(zipFile);
        FileCopyUtils.copy(IOUtils.toByteArray(is), response.getOutputStream());
        is.close();
        response.flushBuffer();
        return resp;
    }
}



